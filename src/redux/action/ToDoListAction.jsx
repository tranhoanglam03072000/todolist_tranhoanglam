import { type } from "@testing-library/user-event/dist/type";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../constant/ToDoListConstand";

export const handleAddTaskAction = (newTask) => ({
  type: ADD_TASK,
  payload: newTask,
});
export const handleChangeTheme = (themeId) => ({
  type: CHANGE_THEME,
  payload: themeId,
});
export const handleDoneTaskAction = (idTask) => ({
  type: DONE_TASK,
  payload: idTask,
});
export const handleDeleteAction = (idTask) => ({
  type: DELETE_TASK,
  payload: idTask,
});
export const handleEditTaskAction = (task) => ({
  type: EDIT_TASK,
  payload: task,
});
export const handleUpDateTask = (id, taskName) => ({
  type: UPDATE_TASK,
  payload: {
    id,
    taskName,
  },
});
