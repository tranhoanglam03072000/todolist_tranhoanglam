import { arrTheme } from "../../theme/ThemeManager";
import { ToDoListDarkTheme } from "../../theme/ToDoListDarkTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../constant/ToDoListConstand";
const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "task 1", done: true },
    { id: 2, taskName: "task 2", done: false },
    { id: 3, taskName: "task 3", done: true },
    { id: 4, taskName: "task 4", done: false },
  ],
  taskEdit: { id: "-1", taskName: "", done: false },
};

export const ToDoListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK:
      {
        if (payload.taskName.trim() == "") {
          alert("Không được gửi task name rỗng");
          return { ...state };
        }
        // ktra tồn tại
        let index = state.taskList.findIndex(
          (task) => task.taskName == payload.taskName
        );
        if (index != -1) {
          alert("Task Name Trùng");
          return { ...state };
        }
        state.taskList.push(payload);
        return { ...state, taskList: [...state.taskList] };
      }
      break;
    case CHANGE_THEME:
      {
        let index = arrTheme.findIndex((theme) => theme.id == payload);
        if (index != -1) {
          state.themeToDoList = arrTheme[index].theme;
        }
        return { ...state };
      }
      break;
    case DONE_TASK:
      {
        let index = state.taskList.findIndex((task) => task.id == payload);
        if (index != -1) {
          state.taskList[index].done = true;
          return { ...state, taskList: [...state.taskList] };
        }
        return { ...state };
      }
      break;
    case DELETE_TASK:
      {
        let newTaskList = state.taskList.filter((task) => task.id != payload);
        return { ...state, taskList: newTaskList };
      }
      break;
    case EDIT_TASK:
      {
        return { ...state, taskEdit: payload };
      }
      break;
    case UPDATE_TASK: {
      let index = state.taskList.findIndex((task) => task.id == payload.id);
      state.taskList[index].taskName = payload.taskName;
      return { ...state, taskList: [...state.taskList] };
    }
    default:
      return state;
  }
};
