import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Button } from "../components/Button";
import { Dropdown } from "../components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../components/Heading";
import { Table, Tr, Td, Th, Thead, Tbody } from "../components/Table";
import { TextField } from "../components/TextField";
import { Container } from "../container/Container";
import {
  handleAddTaskAction,
  handleChangeTheme,
  handleDeleteAction,
  handleDoneTaskAction,
  handleEditTaskAction,
  handleUpDateTask,
} from "../redux/action/ToDoListAction";
import { arrTheme } from "../theme/ThemeManager";
import { ToDoListDarkTheme } from "../theme/ToDoListDarkTheme";
import { ToDoListPrimaryTheme } from "../theme/ToDoListPrimaryTheme";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => task.done == false)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-end">
              <Button
                onClick={() => {
                  this.props.dispatch(handleDeleteAction(task.id));
                }}
              >
                Xoá
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(handleEditTaskAction(task));
                  this.setState({
                    disabled: false,
                  });
                }}
                className="mx-2"
              >
                Sửa
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(handleDoneTaskAction(task.id));
                }}
              >
                Check
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done == true)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-end">
              <Button
                onClick={() => {
                  this.props.dispatch(handleDeleteAction(task.id));
                }}
              >
                Xoá
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option value={theme.id} key={index}>
          {theme.name}
        </option>
      );
    });
  };
  // UNSAFE_componentWillReceiveProps(newProps) {
  //   //nhận vào props mới được thực thi trước render
  //   console.log(newProps.taskEdit);
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }
  // static getDerivedStateFromProps(newProps, currentState) {
  //   console.log(newProps);
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };

  //   return newState;
  //   // trả về null thì giữ nguyên state
  //   // return null;
  // }
  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(handleChangeTheme(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          <TextField
            name="taskName"
            value={this.state.taskName}
            onChange={(e) => {
              this.setState({
                taskName: e.target.value,
              });
            }}
            label="Task Name"
          ></TextField>
          <Button
            disabled={this.state.disabled == true ? false : true}
            onClick={() => {
              console.log("add task");
              let { taskName } = this.state;
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              this.setState(
                {
                  taskName: "",
                },
                () => {
                  this.props.dispatch(handleAddTaskAction(newTask));
                }
              );
            }}
            className="mx-3"
          >
            Thêm Task
          </Button>
          <Button
            disabled={this.state.disabled == true ? true : false}
            onClick={() => {
              console.log("cap nhat");
              let { taskName } = this.state;
              this.setState(
                {
                  taskName: "",
                  disabled: true,
                },
                () => {
                  this.props.dispatch(
                    handleUpDateTask(this.props.taskEdit.id, taskName)
                  );
                }
              );
            }}
          >
            Cập Nhật Task
          </Button>
          <hr />
          <Heading2 className="mt-3">Task To Do</Heading2>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading2>Task ComPlete</Heading2>
          <Table>
            <Thead>{this.renderTaskComplete()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
  componentDidUpdate(preProps, preState) {
    //props cũ , state cũ trước render
    // so sánh nếu như props trước đó (taskEdit trước mà khác taskEdit sau thig mới setState)
    if (preProps.taskEdit.id != this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}
const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};
export default connect(mapStateToProps)(ToDoList);
